#pragma once
#include <glm/glm.hpp>

class Camera
{
	public:
		Camera();
		~Camera();
		glm::vec3 getPosition();
		void setPosition(glm::vec3 new_position);
		float getFocalLength();
		void setFocalLength(float length);

	private:
		float focal_length;
		glm::vec3 position;
};