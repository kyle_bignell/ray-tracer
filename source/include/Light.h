#pragma once
#include <glm/glm.hpp>

class Light
{
	public:
		Light();
		~Light();
		glm::vec3 getPosition();
		void setPosition(glm::vec3 new_position);

	private:
		glm::vec3 position;
};