#pragma once
#include <glm/glm.hpp>

class Image
{
	public:
		Image();
		~Image();
		glm::vec3 getPosition();
		void setPosition(glm::vec3 new_position);
		int getWidth();
		void setWidth(int new_width);
		int getHeight();
		void setHeight(int new_height);
		glm::vec3** getPixels();
		void resetPixels();
		void setPixel(int height, int width, glm::vec3 color);

	private:
		glm::vec3 position;;
		int width;
		int height;
		glm::vec3** pixels;
};

