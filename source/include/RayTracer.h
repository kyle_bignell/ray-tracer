#pragma once
#include <glm/glm.hpp>

class RayTracer
{
	public:
		RayTracer();
		~RayTracer();
		glm::vec3 generateRay(glm::vec3 camera, glm::vec3 pixel);

	private:
		
};

