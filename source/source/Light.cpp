#include "Light.h"


Light::Light()
{
	position = glm::vec3(0.0f, 0.0f, 0.0f);
}

Light::~Light()
{
}

glm::vec3 Light::getPosition()
{
	return position;
}

void Light::setPosition(glm::vec3 new_position)
{
	position = new_position;
}