#include "Camera.h"


Camera::Camera()
{
	focal_length = 10.0f;
	position = glm::vec3(0.0f, 0.0f, 0.0f);
}

Camera::~Camera()
{
}

glm::vec3 Camera::getPosition()
{
	return position;
}

void Camera::setPosition(glm::vec3 new_position)
{
	position = new_position;
}

float Camera::getFocalLength()
{
	return focal_length;
}

void Camera::setFocalLength(float length)
{
	focal_length = length;
}