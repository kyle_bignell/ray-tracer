#include <SDL.h>
#include <stdio.h>
#include <assert.h>
#include <iostream>

#include "Camera.h"
#include "Image.h"
#include "Light.h"
#include "tiny_obj_loader.h"

//Screen dimension constants
const int SCREEN_WIDTH = 720;
const int SCREEN_HEIGHT = 720;

void print_object_details(std::vector<tinyobj::shape_t> shapes)
{
	for (size_t i = 0; i < shapes.size(); i++)
	{
		printf("shape[%ld].name = %s\n", i, shapes[i].name.c_str());

		printf("shape[%ld].vertices: %ld\n", i, shapes[i].mesh.positions.size());
		assert((shapes[i].mesh.positions.size() % 3) == 0);
		assert((shapes[i].mesh.indices.size() % 3) == 0);

		for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++)
		{
			printf("  v[%ld] = (%f, %f, %f)\n",
				v,
				shapes[i].mesh.positions[3 * v + 0],
				shapes[i].mesh.positions[3 * v + 1],
				shapes[i].mesh.positions[3 * v + 2]);
		}

		printf("shape[%ld].normals: %ld\n", i, shapes[i].mesh.normals.size());
		assert((shapes[i].mesh.normals.size() % 3) == 0);
		assert((shapes[i].mesh.indices.size() % 3) == 0);

		for (size_t v = 0; v < shapes[i].mesh.normals.size() / 3; v++)
		{
			printf("  v[%ld] = (%f, %f, %f)\n",
				v,
				shapes[i].mesh.normals[3 * v + 0],
				shapes[i].mesh.normals[3 * v + 1],
				shapes[i].mesh.normals[3 * v + 2]);
		}
	}
}

void print_camera_details(Camera camera)
{
	printf("Camera position (%f, %f, %f)\n", camera.getPosition().x, camera.getPosition().y, camera.getPosition().z);
	printf("Camera focal length %f\n", camera.getFocalLength());
}

void print_image_details(Image image)
{
	printf("Screen width %d\n", image.getWidth());
	printf("Screen height %d\n", image.getHeight());
}

void print_light_details(Light light)
{
	printf("Light position (%f, %f, %f)\n", light.getPosition().x, light.getPosition().y, light.getPosition().z);
}

void load_objects(std::vector<tinyobj::shape_t>* shapes,
			      std::vector<tinyobj::material_t>* materials)
{

	std::vector<tinyobj::shape_t> plane;
	std::vector<tinyobj::material_t> plane_materials;
	std::vector<tinyobj::shape_t> cube;
	std::vector<tinyobj::material_t> cube_materials;
	std::vector<tinyobj::shape_t> cylinder;
	std::vector<tinyobj::material_t> cylinder_materials;
	std::string err;

	err += tinyobj::LoadObj(plane, plane_materials, "models/plane.obj");
	err += tinyobj::LoadObj(cube, cube_materials, "models/cube.obj");
	err += tinyobj::LoadObj(cylinder, cylinder_materials, "models/cylinder.obj");

	if (!err.empty())
	{
		std::cerr << err << std::endl;
		exit(1);
	}

	shapes->reserve(plane.size() + cube.size() + cylinder.size());
	shapes->insert(shapes->end(), plane.begin(), plane.end());
	shapes->insert(shapes->end(), cube.begin(), cube.end());
	shapes->insert(shapes->end(), cylinder.begin(), cylinder.end());

	materials->reserve(plane_materials.size() + cube_materials.size() + cylinder_materials.size());
	materials->insert(materials->end(), plane_materials.begin(), plane_materials.end());
	materials->insert(materials->end(), cube_materials.begin(), cube_materials.end());
	materials->insert(materials->end(), cylinder_materials.begin(), cylinder_materials.end());
}

// Ray triangle intersection - Algorithm publish in "Fast, Minimum Storage Ray/Triangle Intersetion"
// by Tomas Moller and Ben Trumbore
int intersect_triangle(glm::vec3 origin, glm::vec3 direction,
					   glm::vec3 vert0, glm::vec3 vert1, glm::vec3 vert2,
					   float* u, float* v, float* distance)
{
	glm::vec3 edge1;
	glm::vec3 edge2;
	glm::vec3 tvec;
	glm::vec3 pvec;
	glm::vec3 qvec;
	float det;
	float inv_det;
	bool cull = true;

	edge1 = vert1 - vert0;
	edge2 = vert2 - vert0;

	pvec = glm::cross(direction, edge2);

	det = glm::dot(pvec, edge1);

	// Back face culling
	if (cull)
	{
		if (det < 0.000001)
		{
			return 0;
		}

		tvec = origin - vert0;

		*u = glm::dot(tvec, pvec);
		if (*u < 0.0 || *u > det)
		{
			return 0;
		}

		qvec = glm::cross(tvec, edge1);

		*v = glm::dot(direction, qvec);
		if (*v < 0.0 || *u + *v > det)
		{
			return 0;
		}

		*distance = glm::dot(edge2, qvec);
		inv_det = 1.0f / det;
		*distance *= inv_det;
		*u *= inv_det;
		*v *= inv_det;
	}
	// No back face culling
	else
	{
		if (det > -0.000001 && det < 0.000001)
		{
			return 0;
		}

		inv_det = 1.0f / det;

		tvec = origin - vert0;

		*u = glm::dot(tvec, pvec) * inv_det;
		if (*u < 0.0 || *u > 1.0)
		{
			return 0;
		}

		qvec = glm::cross(tvec, edge1);

		*v = glm::dot(direction, qvec) * inv_det;
		if (*v < 0.0 || *u + *v > 1.0)
		{
			return 0;
		}

		*distance = glm::dot(edge2, qvec) * inv_det;
	}

	return 1;
}

// Lambertian shading
float shade_diffuse(glm::vec3 normal, glm::vec3 light_ray)
{
	float diffuse_coefficient = 1.0f;
	normal = glm::normalize(normal);
	light_ray = glm::normalize(light_ray);

	float diffuse_light = diffuse_coefficient * glm::max(0.0f, glm::dot(normal, light_ray));

	return diffuse_light;
}

// Blinn-Phong shading
float shade_specular(glm::vec3 normal, glm::vec3 bisector_ray)
{
	float specular_coefficient = 1.0f;
	normal = glm::normalize(normal);
	bisector_ray = glm::normalize(bisector_ray);

	float specular_light = specular_coefficient * glm::pow(glm::max(0.0f, glm::dot(normal, bisector_ray)), 4);

	return specular_light;
}

int main(int argc, char* args[])
{
	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return -1;
	}

	//Create window
	window = SDL_CreateWindow("Ray Tracer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (window == NULL)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		return -1;
	}

	//Create renderer for window
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (renderer == NULL)
	{
		printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
		return -1;
	}

	// Load scene objects
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	load_objects(&shapes, &materials);

	Camera camera;
	camera.setPosition(glm::vec3(0.0f, 5.0f, -25.0f));
	camera.setFocalLength(2.5f);

	Image image;
	image.setWidth(SCREEN_WIDTH);
	image.setHeight(SCREEN_HEIGHT);
	image.resetPixels();

	Light light_1;
	light_1.setPosition(glm::vec3(10.0f, 10.0f, -10.0f));
	Light light_2;
	light_2.setPosition(glm::vec3(10.0f, 10.0f, -10.0f));

	print_object_details(shapes);
	print_camera_details(camera);
	print_image_details(image);
	print_light_details(light_1);
	print_light_details(light_2);

	float left = 0 - 1;
	float right = 0 + 1;
	float width = (float) image.getWidth();
	float bottom = 0 - 1;
	float top = 0 + 1;
	float height = (float) image.getHeight();

	// Distance along ray that it intersects with an object
	float ray_distance;
	float distance;
	float u;
	float v;
	float diffuse;
	float specular;
	float ambient = 0.1f;
	float shadow;
	float lighting;
	int intersect;

	int point_index_1;
	int point_index_2;
	int point_index_3;
	glm::vec3 point_1;
	glm::vec3 point_2;
	glm::vec3 point_3;
	glm::vec3 normal_ray_1;
	glm::vec3 normal_ray_2;
	glm::vec3 normal_ray_3;
	glm::vec3 normal_ray;
	glm::vec3 light_ray_1;
	glm::vec3 light_ray_2;
	glm::vec3 origin_point;
	glm::vec3 view_ray;
	glm::vec3 bisector;
	glm::mat3x3 rotation;

	for (int r = 0; r < 7; r++)
	{
		rotation = glm::fmat3x3(glm::cos(r), 0, glm::sin(r), 0, 1, 0, -glm::sin(r), 0, glm::cos(r));

		// Loop through each pixel in the image
		for (int y = 0; y < image.getHeight(); ++y)
		{
			for (int x = 0; x < image.getWidth(); ++x)
			{
				// Create direction ray for each pixel
				glm::vec3 ray = glm::vec3(0.0f);
				ray.x = (float)(left + ((right - left) * (x + 0.5)) / width);
				ray.y = (float)(bottom + ((top - bottom) * (y + 0.5)) / height);
				ray.z = (float)camera.getFocalLength();

				// Distance along ray that it intersects with an object
				ray_distance = -1.0;

				// Loop through all shape objects in the scene
				for (size_t i = 0; i < shapes.size(); i++)
				{
					// Loop through each triangle of current shape
					for (size_t f = 0; f < shapes[i].mesh.indices.size() / 3; f++)
					{
						// Get index points for position array
						point_index_1 = shapes[i].mesh.indices[(3 * f) + 0];
						point_index_2 = shapes[i].mesh.indices[(3 * f) + 1];
						point_index_3 = shapes[i].mesh.indices[(3 * f) + 2];

						// Get points of triangle and rotate by rotation matrix
						point_1 = glm::fvec3(shapes[i].mesh.positions[(3 * point_index_1) + 0],
											 shapes[i].mesh.positions[(3 * point_index_1) + 1],
											 shapes[i].mesh.positions[(3 * point_index_1) + 2]);
						point_1 = rotation * point_1;

						point_2 = glm::fvec3(shapes[i].mesh.positions[(3 * point_index_2) + 0],
											 shapes[i].mesh.positions[(3 * point_index_2) + 1],
											 shapes[i].mesh.positions[(3 * point_index_2) + 2]);
						point_2 = rotation * point_2;

						point_3 = glm::fvec3(shapes[i].mesh.positions[(3 * point_index_3) + 0],
											 shapes[i].mesh.positions[(3 * point_index_3) + 1],
											 shapes[i].mesh.positions[(3 * point_index_3) + 2]);
						point_3 = rotation * point_3;

						// Check if ray intersects triangle
						// u & v are barymetric coordinates
						intersect = intersect_triangle(camera.getPosition(), ray, point_1, point_2, point_3,
													   &u, &v, &distance);

						// No intersection
						if (intersect != 1)
						{
							continue;
						}
						// Intersected a triangle that is closer than previous intersection
						else if ((ray_distance == -1.0f && distance >= 0.0) || distance <= ray_distance)
						{
							ray_distance = distance;
						}
						// Intersection but it is not the closest
						else
						{
							continue;
						}

						//Shading

						// Retrieve vertex normals and rotate them by rotation matrix
						normal_ray_1 = glm::vec3(shapes[i].mesh.normals[(3 * point_index_1) + 0],
												 shapes[i].mesh.normals[(3 * point_index_1) + 1],
												 shapes[i].mesh.normals[(3 * point_index_1) + 2]);
						normal_ray_1 = rotation * normal_ray_1;

						normal_ray_2 = glm::vec3(shapes[i].mesh.normals[(3 * point_index_2) + 0],
												 shapes[i].mesh.normals[(3 * point_index_2) + 1],
												 shapes[i].mesh.normals[(3 * point_index_2) + 2]);
						normal_ray_2 = rotation * normal_ray_2;

						normal_ray_3 = glm::vec3(shapes[i].mesh.normals[(3 * point_index_3) + 0],
												 shapes[i].mesh.normals[(3 * point_index_3) + 1],
												 shapes[i].mesh.normals[(3 * point_index_3) + 2]);
						normal_ray_3 = rotation * normal_ray_3;

						// Interpolate normal using barycentric coordinates
						normal_ray = (((1 - u) - v) * normal_ray_1) + (u * normal_ray_2) + (v * normal_ray_3);

						// Calculate rays for light sources
						light_ray_1 = glm::normalize(light_1.getPosition() - (camera.getPosition() + (distance * ray)));
						light_ray_2 = glm::normalize(light_2.getPosition() - (camera.getPosition() + (distance * ray)));

						// Diffuse shading
						diffuse = 0.0f;
						diffuse += shade_diffuse(normal_ray, light_ray_1);
						diffuse += shade_diffuse(normal_ray, light_ray_2);
						// Average results by number of light sources
						diffuse /= 2;
						diffuse = glm::min(1.0f, diffuse);

						// Specular shading
						origin_point = camera.getPosition() + (distance * ray);
						view_ray = glm::normalize(camera.getPosition() - origin_point);
						specular = 0.0f;
						bisector = (view_ray + light_ray_1) / (glm::length(view_ray + light_ray_1));
						specular += shade_specular(normal_ray, bisector);
						bisector = (view_ray + light_ray_2) / (glm::length(view_ray + light_ray_2));
						specular += shade_specular(normal_ray, bisector);
						// Average results by number of light sources
						specular /= 2;
						specular = glm::min(1.0f, specular);

						// Shadows
						shadow = 1;

						// Loop through all shape objects in the scene
						for (size_t h = 0; h < shapes.size(); h++)
						{
							// Don't check shadows against yourself
							if (shapes[i].name != shapes[h].name)
							{
								// Loop through each triangle of current shape
								for (size_t m = 0; m < shapes[h].mesh.indices.size() / 3; m++)
								{
									point_index_1 = shapes[h].mesh.indices[(3 * m) + 0];
									point_index_2 = shapes[h].mesh.indices[(3 * m) + 1];
									point_index_3 = shapes[h].mesh.indices[(3 * m) + 2];

									point_1 = glm::vec3(shapes[h].mesh.positions[(3 * point_index_1) + 0],
														shapes[h].mesh.positions[(3 * point_index_1) + 1],
														shapes[h].mesh.positions[(3 * point_index_1) + 2]);
									point_1 = rotation * point_1;

									point_2 = glm::vec3(shapes[h].mesh.positions[(3 * point_index_2) + 0],
														shapes[h].mesh.positions[(3 * point_index_2) + 1],
														shapes[h].mesh.positions[(3 * point_index_2) + 2]);
									point_2 = rotation * point_2;

									point_3 = glm::vec3(shapes[h].mesh.positions[(3 * point_index_3) + 0],
														shapes[h].mesh.positions[(3 * point_index_3) + 1],
														shapes[h].mesh.positions[(3 * point_index_3) + 2]);
									point_3 = rotation * point_3;

									// Check for shadow intersection
									intersect = intersect_triangle(origin_point, light_ray_1,
																   point_1, point_2, point_3,
																   &u, &v, &distance);

									// Hit a triangle, increase shadow level
									if (intersect == 1)
									{
										shadow -= 0.4f;
									}

									// Repeat for second light source
									intersect = intersect_triangle(origin_point, light_ray_2,
																   point_1, point_2, point_3,
																   &u, &v, &distance);

									if (intersect == 1)
									{
										shadow -= 0.4f;
									}
								}
							}
						}

						// Cap shadow value to prevent negative values
						shadow = glm::max(0.2f, shadow);

						// Set pixel colour
						if (shapes[i].name == "Plane")
						{
							// Add specular white highlights to colour of shape
							glm::vec3 specular_vec = glm::vec3(255.0f, 255.0f, 255.0f) * specular;
							glm::vec3 colour_vec = glm::vec3(255.0, 255.0, 255.0);
							// Cap colour values to prevent oversaturation
							colour_vec.x = glm::min(255.0f, colour_vec.x + specular_vec.x);
							colour_vec.y = glm::min(255.0f, colour_vec.y + specular_vec.y);
							colour_vec.z = glm::min(255.0f, colour_vec.z + specular_vec.z);

							lighting = glm::min(1.0f, (diffuse + ambient)) * shadow;
							image.setPixel(y, x, colour_vec * lighting);
						}
						else if (shapes[i].name == "Cylinder")
						{
							// Add specular white highlights to colour of shape
							glm::vec3 specular_vec = glm::vec3(255.0f, 255.0f, 255.0f) * specular;
							glm::vec3 colour_vec = glm::vec3(255.0, 0, 0);
							// Cap colour values to prevent oversaturation
							colour_vec.x = glm::min(255.0f, colour_vec.x + specular_vec.x);
							colour_vec.y = glm::min(255.0f, colour_vec.y + specular_vec.y);
							colour_vec.z = glm::min(255.0f, colour_vec.z + specular_vec.z);

							// Calculate 
							lighting = glm::min(1.0f, (diffuse + ambient)) * shadow;
							image.setPixel(y, x, colour_vec * lighting);
						}
						else if (shapes[i].name == "Cube")
						{
							// Add specular white highlights to colour of shape
							glm::vec3 specular_vec = glm::vec3(255.0f, 255.0f, 255.0f) * specular;
							glm::vec3 colour_vec = glm::vec3(0, 0, 255.0);
							// Cap colour values to prevent oversaturation
							colour_vec.x = glm::min(255.0f, colour_vec.x + specular_vec.x);
							colour_vec.y = glm::min(255.0f, colour_vec.y + specular_vec.y);
							colour_vec.z = glm::min(255.0f, colour_vec.z + specular_vec.z);

							lighting = glm::min(1.0f, (diffuse + ambient)) * shadow;
							image.setPixel(y, x, colour_vec * lighting);
						}
					}
				}
			}
		}

		// Clear the render target
		SDL_RenderClear(renderer);

		// Loop through pixels and draw them to screen
		glm::vec3** pixels = image.getPixels();
		for (int i = 0; i < image.getHeight(); i++)
		{
			for (int j = 0; j < image.getWidth(); j++)
			{
				SDL_SetRenderDrawColor(renderer, (int) pixels[i][j].x, (int) pixels[i][j].y, (int) pixels[i][j].z, 255);
				SDL_RenderDrawPoint(renderer, j, SCREEN_HEIGHT - i);
			}
		}

		SDL_Surface *sshot = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
		SDL_RenderReadPixels(renderer, NULL, SDL_PIXELFORMAT_ARGB8888, sshot->pixels, sshot->pitch);
		std::string output = "screenshot_" + std::to_string(r) + ".bmp";
		SDL_SaveBMP(sshot, output.c_str());
		SDL_FreeSurface(sshot);
		SDL_RenderPresent(renderer);

		image.resetPixels();
	}
	
	std::cin.ignore();

	SDL_DestroyWindow(window);

	SDL_Quit();

	return 0;
}