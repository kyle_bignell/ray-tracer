#include "Image.h"


Image::Image()
{
}

Image::~Image()
{
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	width = 0;
	height = 0;
	pixels = NULL;
}

glm::vec3 Image::getPosition()
{
	return position;
}

void Image::setPosition(glm::vec3 new_position)
{
	position = new_position;
}

int Image::getWidth()
{
	return width;
}

void Image::setWidth(int new_width)
{
	width = new_width;
}

int Image::getHeight()
{
	return height;
}

void Image::setHeight(int new_height)
{
	height = new_height;
}

glm::vec3** Image::getPixels()
{
	return pixels;
}

void Image::resetPixels()
{
	pixels = new glm::vec3*[height];
	for (int i = 0; i < height; ++i)
	{
		pixels[i] = new glm::vec3[width];
	}
}

void Image::setPixel(int height, int width, glm::vec3 color)
{
	pixels[height][width] = color;
}